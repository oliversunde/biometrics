import seaborn as sn
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# Read CSV file using pandas

# datapoints with 15 decimals
dataasian=[20.12...25.123451234512345]#500, too long for this document
datablack=[] # up to 500 datapoints...
dataindian=[]
datalatino=[]
datameastern[]
datawhite[]

#average
print(sum(dataasian)/len(dataasian))
print(sum(datablack)/len(datablack))
print(sum(dataindian)/len(dataindian))
print(sum(datalatino)/len(datalatino))
print(sum(datameastern)/len(datameastern))
print(sum(datawhite)/len(datawhite))

sn.kdeplot(data=dataasian, color="#d0384e")
sn.kdeplot(data=datablack, color="#fa9b58")
sn.kdeplot(data=dataindian, color="#fff1a8")
sn.kdeplot(data=datalatino, color="#d1ed9c")
sn.kdeplot(data=datameastern, color="#5cb7aa")
sn.kdeplot(data=datawhite, color="#3682ba")
plt.legend(labels=["asian","black","indian","latino","middle eastern","white"])
plt.show()
