import array
from deepface import DeepFace
import os
import array as arr
from os import listdir
import glob
import shutil

directory =os.getcwd()
fitt=arr.array('B')
i=0
rase=[]
os.makedirs('middle eastern')
os.makedirs('white')
os.makedirs('indian')
os.makedirs('black')
os.makedirs('latino hispanic')
os.makedirs('asian')

for images in os.listdir(directory):
	if images.endswith(".png"):
		obj = DeepFace.analyze(img_path = images, actions =['race'],
		enforce_detection=False)
		ras=obj['dominant_race']
		rase.append(ras)
		shutil.copy(images, directory + '/' + ras)
